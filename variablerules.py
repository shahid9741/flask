from flask import Flask

app = Flask (__name__)

@app.route('/hello/<shahid>')
def Hello_world(shahid):
    return 'Hello World %s!' % shahid

@app.route('/blog/<int:shahid>')
def show_blog(shahid):
    return 'Blog Number %d' % shahid

@app.route('/rev/<float:shahid>')
def rev(shahid):
    return 'Revsion number %f' % shahid

if __name__=='__main__':
    app.run(debug=True)