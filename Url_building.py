from flask import Flask, redirect, url_for
app = Flask(__name__)

@app.route('/admin')
def hello_admin():
    return 'Hello admin'

@app.route('/guest/<shahid>')
def hello_guest(shahid):
    return "Hello %s " % shahid

@app.route('/user/<khan>')
def hello_user(khan):
    if khan == 'admin':
        return redirect(url_for('hello_admin'))
    else:
        return redirect(url_for('hello_guest', shahid = khan))
    
if __name__ == "__main__":
    app.run(debug=True)