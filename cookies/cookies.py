from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def index():
   return render_template('index.html')

@app.route('/setcookie', methods=['POST', 'GET'])
def setcookie():
	if request.method=='POST':
		user = request.form['nm']
	response = make_response(render_template('readcookie.html'))
	response.set_cookie('userID', user)
	return response

#create readcookie.html which has hyperlink
@app.route('/getcookie')
def getcookie():
	name = request.cookies.get('userID')
	return '<h1>welcome '+name+'</h1>'

if __name__ == '__main__':
	app.run()